#include "MeAuriga.h"

MeLEDMatrix matrix = MeLEDMatrix(PORT_10);
uint8_t bitmap[16] = { 0 };

void setPixel(int x, int y) {
  bitmap[x] |= 1 << y;
}

void setup() {
  setPixel(2, 7);
  setPixel(3, 7);
  setPixel(11, 7);
  setPixel(12, 7);
  // row 1
  setPixel(1, 6);
  setPixel(4, 6);
  setPixel(10, 6);
  setPixel(13, 6);
  // row 2
  setPixel(1, 5);
  setPixel(2, 5);
  setPixel(4, 5);
  setPixel(10, 5);
  setPixel(11, 5);
  setPixel(12, 5);
  setPixel(13, 5);
  // row 3
  setPixel(2, 4);
  setPixel(3, 4);
  setPixel(7, 4);
  setPixel(8, 4);
  setPixel(11, 4);
  setPixel(12, 4);
  // row 4
  setPixel(7, 3);
  // row 5
  setPixel(7, 2);
  setPixel(8, 2);
  setPixel(12, 2);
  // row 6
  setPixel(2, 1);
  setPixel(3, 1);
  setPixel(10, 1);
  setPixel(11, 1);
  // row 6
  setPixel(4, 0);
  setPixel(5, 0);
  setPixel(6, 0);
  setPixel(7, 0);
  setPixel(8, 0);
  setPixel(9, 0);
  matrix.drawBitmap(0, 0, 16, bitmap);
}

void loop() {
}
