#include "MeAuriga.h"

MeLEDMatrix matrix = MeLEDMatrix(PORT_10);
uint8_t bitmap[16] = { 0 };

void setPixel(int x, int y) {
  bitmap[x] |= 1 << y;
}

void setup() {
  setPixel(7, 3);
  setPixel(7, 5);
  matrix.drawBitmap(0, 0, 16, bitmap);
}

void loop() {
}
