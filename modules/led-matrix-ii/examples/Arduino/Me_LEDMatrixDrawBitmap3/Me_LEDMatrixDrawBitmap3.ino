#include "MeAuriga.h"

MeLEDMatrix matrix = MeLEDMatrix(PORT_10);

void setup() {
  uint8_t bitmap[16] = { 0 };
  int x = 7;
  int y0 = 3;
  int y1 = 5;

  bitmap[x] = 1 << y0;
  bitmap[x] |= 1 << y1;
  matrix.drawBitmap(0, 0, 16, bitmap);
}

void loop() {
}