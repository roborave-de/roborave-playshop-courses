#include "MeAuriga.h"

MeLEDMatrix matrix = MeLEDMatrix(PORT_10);
uint8_t bitmap[16] = { 0 };

void setPixel(int x, int y) {
  bitmap[x] |= 1 << y;
}

void setup() {
  // row 1
  setPixel(2, 6);
  setPixel(3, 6);
  setPixel(4, 6);
  setPixel(5, 6);
  setPixel(6, 6);
  setPixel(9, 6);
  setPixel(10, 6);
  setPixel(11, 6);
  setPixel(12, 6);
  setPixel(13, 6);
  // row 2
  setPixel(4, 5);
  setPixel(5, 5);
  setPixel(10, 5);
  setPixel(11, 5);
  // row 3
  setPixel(4, 4);
  setPixel(5, 4);
  setPixel(10, 4);
  setPixel(11, 4);
  // row 5
  setPixel(3, 2);
  setPixel(12, 2);
  // row 6
  setPixel(3, 1);
  setPixel(12, 1);
  // row 7
  setPixel(4, 0);
  setPixel(5, 0);
  setPixel(6, 0);
  setPixel(7, 0);
  setPixel(8, 0);
  setPixel(9, 0);
  setPixel(10, 0);
  setPixel(11, 0);
  matrix.drawBitmap(0, 0, 16, bitmap);
}

void loop() {
}
