[frame=none, grid=cols, cols=">1,<3,<3", stripes=hover]
|===
| Operator | Bedeutung                  | Beispiel

| `<<`     | Verschiebung nach links    | `00111100 << 2 = 11110000`
| `>>`     | Verschiebung nach rechts  a| * `11110000 >> 2 = 00111100` (vorzeichenlos)
                                          * `11110000 >> 2 = 11111100` (mit Vorzeichen)
|===
