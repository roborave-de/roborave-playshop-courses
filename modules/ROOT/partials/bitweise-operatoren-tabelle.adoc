[frame=none, grid=cols, cols=">1,<3,<3", stripes=hover]
|===
| Operator | Bedeutung             | Beispiel

| `~`      | NICHT (engl. _NOT_)   | `~00111100 = 11000011`
| `&`      | UND (engl. _AND_)     | `00111100 & 11110000 = 00110000`
| `\|`     | ODER (engl. _OR_)     | `00111100 & 11110000 = 11110000`
| `^`      | XOR (engl. _XOR_)     | `00111100 & 11110000 = 11000000`
|===
