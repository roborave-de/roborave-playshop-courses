#include "MeAuriga.h"

MeLEDMatrix matrix = MeLEDMatrix(PORT_10);
char text[] = "   ABCDEFGHIJKLMNOPQRSTUVWXYZ";
int len = strlen(text);
int n_led = 6;

void setup() {
}

void loop() {
  for (int c = 0; c < len; c++) {
    for (int x = 0; x > -n_led; x--) {
      matrix.drawStr(x, 7, &text[c]);
      delay(60);
    }
  }
}
