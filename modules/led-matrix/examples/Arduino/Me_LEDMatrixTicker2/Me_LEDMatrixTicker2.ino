#include "MeAuriga.h"

MeLEDMatrix matrix = MeLEDMatrix(PORT_10);
char text[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
int len = strlen(text);
int n_led = len * 6;

void setup() {
}

void loop() {
  for (int x = 15; x > -n_led; x--) {
    matrix.drawStr(x, 7, text);
    delay(60);
  }
}
