#include "MeAuriga.h"

MeLEDMatrix matrix = MeLEDMatrix(PORT_10);
char text[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
int len = strlen(text);

void setup() {
}

void loop() {
  for (int c = 0; c < len; c++) {
    matrix.drawStr(0, 7, &text[c]);
    delay(360);
  }
}