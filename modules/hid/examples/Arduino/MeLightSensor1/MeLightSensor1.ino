#include "MeAuriga.h"

MeLightSensor ls = MeLightSensor();

void setup() {
  Serial.begin(9600);
  ls.setpin(0, A3);
}

void loop() {
  int16_t value;

  value = ls.read();
  Serial.print("Wert = ");
  Serial.println(value);
  delay(100);
}