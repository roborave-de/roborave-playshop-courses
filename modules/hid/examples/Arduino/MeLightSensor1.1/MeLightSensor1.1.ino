#include "MeAuriga.h"

MeLightSensor ls1 = MeLightSensor();
MeLightSensor ls2 = MeLightSensor();

void setup() {
  Serial.begin(9600);
  ls1.setpin(0, A3);
  ls2.setpin(0, A2);
}

void loop() {
  int16_t val1 = 0;
  int16_t val2 = 0;

  val1 = ls1.read();
  val2 = ls2.read();
  Serial.print("Wert 1 = ");
  Serial.print(val1);
  Serial.print("; Wert 2 = ");
  Serial.println(val2);
  delay(100);
}