#include "MeAuriga.h"

MeLightSensor ls1 = MeLightSensor();
MeLightSensor ls2 = MeLightSensor();

int16_t val1 = 0;
int16_t val2 = 0;
int16_t diff;

enum ls {
  LS1,
  LS2,
};

enum ls getLightSensor(void) {
  val1 = ls1.read();
  val2 = ls2.read();

  Serial.print("ls1 = ");
  Serial.println(val1);
  Serial.print("ls2 = ");
  Serial.println(val2);

  val1 += diff;

  if (val1 < val2) {
    return LS1;
  }
  return LS2;
}

void setup() {
  Serial.begin(9600);
  ls1.setpin(0, A3);
  ls2.setpin(0, A2);
  val1 = ls1.read();
  val2 = ls2.read();

  diff = val2 - val1;
}

void loop() {
  enum ls ls;

  ls = getLightSensor();

  switch (ls) {
    case LS1:
      Serial.println("LS1");
      break;
    case LS2:
      Serial.println("LS2");
      break;
  }
  delay(100);
}