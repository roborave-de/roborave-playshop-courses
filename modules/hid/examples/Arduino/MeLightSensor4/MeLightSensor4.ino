#include "MeAuriga.h"

MeLightSensor ls1 = MeLightSensor();
MeLightSensor ls2 = MeLightSensor();

const int16_t limit = 50;

enum ls {
  NONE,
  LS1,
  LS2,
};

enum ls getLightSensor(void) {
  int16_t val1, val2;

  val1 = ls1.read();
  val2 = ls2.read();

  Serial.print("ls1  = ");
  Serial.println(val1);
  Serial.print("ls2 = ");
  Serial.println(val2);

  if (val1 + limit < val2) {
    return LS1;
  } else if (val2 + limit < val1) {
    return LS2;
  }
  return NONE;
}

void setup() {
  Serial.begin(9600);
  ls1.setpin(0, A3);
  ls2.setpin(0, A2);
}

void loop() {
  enum ls ls;

  ls = getLightSensor();

  switch (ls) {
    case NONE:
      Serial.println("NONE");
      break;
    case LS1:
      Serial.println("LS1");
      break;
    case LS2:
      Serial.println("LS2");
      break;
  }
  delay(100);
}